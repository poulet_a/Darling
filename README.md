# Darling

Darling is a personnal programming assisant.


## Installation

Requires ``libnotify``.

```
make
```


## Usage

```
./Darling
```


## Development

TODO: Write development instructions here


## Contributing

1. Fork it ( https://github.com/Nephos/Darling/fork )
2. Create your feature branch (git checkout -b my-new-feature)
3. Commit your changes (git commit -am 'Add some feature')
4. Push to the branch (git push origin my-new-feature)
5. Create a new Pull Request

## Contributors

- [Nephos](https://github.com/Nephos) Arthur Poulet - creator, maintainer
